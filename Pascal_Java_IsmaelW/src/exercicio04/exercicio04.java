package exercicio04;
import java.util.Scanner;

/**
 * @author 2018101710 - IsmaelW
 */
public class exercicio04 {

    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        float pot_lamp,larg_com,comp_com,area_com,pot_total;
        int num_lamp;

        System.out.println("qual a potencia da lampada (em watts)?");
        pot_lamp=teclado.nextFloat();

        System.out.println("qual a largura do comodo (em metros)?");
        larg_com=teclado.nextFloat();

        System.out.println("qual o comprimento do comodo (em metros)?");
        comp_com=teclado.nextFloat();

        area_com=larg_com*comp_com;
        pot_total=area_com*18;
        num_lamp=Math.round(pot_total/pot_lamp);

        System.out.println("numero de lampadas necessarias para iluminar esse comodo: "+num_lamp);
    }
}
