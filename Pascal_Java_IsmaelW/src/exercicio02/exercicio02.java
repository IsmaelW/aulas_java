package exercicio02;
import java.util.Scanner;

/**
 * @author 2018101710 - IsmaelW
 */
public class exercicio02 {

    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        float temp_f=0,temp_c=0;
        
        System.out.println("Informe a temperatura em graus Fahrenheit: ");
        temp_f=teclado.nextFloat();
        temp_c=((temp_f - 32) * 5) / 9;
        System.out.println("A temperatura em graus Celsius eh: "+temp_c);
        
    }
}