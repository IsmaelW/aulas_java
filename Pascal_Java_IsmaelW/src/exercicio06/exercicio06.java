package exercicio06;
import java.util.Scanner;
/**
 *
 * @author 2018101710 - IsmaelW
 */
public class exercicio06 {
    public static void main(String[] args) {
      Scanner teclado = new Scanner(System.in); 
      float odom_i=0, odom_f=0, litros=0, valor_t=0, media=0, lucro=0, gasol_l=1.90f; 
       
       
      System.out.println("Marcacao inicial do odometro (Km):");
      odom_i=teclado.nextFloat();
      System.out.println("Marcacao final do odometro (Km):");
      odom_f=teclado.nextFloat();
      System.out.println("Quantidade de combustivel gasto (litros):");
      litros=teclado.nextFloat();
      System.out.println("Valor total recebido (R$): ");
      valor_t=teclado.nextFloat();
      
      media=(odom_f-odom_i)/litros;
      lucro=valor_t-(litros*gasol_l);
      
      System.out.println("Media de consumo em Km/L:"+media);
      System.out.println("Lucro (liquido) do dia: R$"+lucro);      
    }
}

/*

writeln ('Media de consumo em Km/L: ', media:4:1);
writeln;
writeln ('Lucro (liquido) do dia: R$',lucro:8:2);
writeln;
*/