package exercicio03;
import java.util.Scanner;

/**
 * @author 2018101710 - IsmaelW
 */
public class exercicio03 {

    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        float temp_f=0,temp_c=0;
        
        System.out.println("Informe a temperatura em graus Celsius: ");
        temp_c=teclado.nextFloat();
        temp_f=(temp_c*9/5)+32;
        System.out.println("A temperatura em graus Fahrenheit eh: "+temp_f);
        
    }
}