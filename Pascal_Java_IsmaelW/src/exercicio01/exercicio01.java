package exercicio01;
import java.util.Scanner;

/**
 * @author 2018101710 - IsmaelW
 */
public class exercicio01 {

    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        float raio=0,area=0;
        
        System.out.println("Informe o raio do circulo: ");
        raio=teclado.nextFloat();
        area=(float) (Math.pow(raio,2)*3.14);
        
        System.out.println("A area do circulo eh:"+area);
    }
    
}
