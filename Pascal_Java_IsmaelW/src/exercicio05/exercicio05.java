package exercicio05;
import java.util.Scanner;
/**
 *
 * @author 2018101710 - IsmaelW
 */
public class exercicio05 {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        float comp=0,larg=0,alt=0,area=0;
        int caixas=0;
        
        System.out.println("qual a comprimento da cozinha?");
        comp=teclado.nextFloat();
        
        System.out.println("qual o largura da cozinha?");
        larg=teclado.nextFloat();
        
        System.out.println("qual a altura da cozinha?");
        alt=teclado.nextFloat();
        
        area=(comp*alt*2)+(larg*alt*2);
        caixas=(int)Math.round(area/1.5);
        
        System.out.println("quantidade de caixas de azulejos para colocar em todas as paredes: "+caixas);
    }
}